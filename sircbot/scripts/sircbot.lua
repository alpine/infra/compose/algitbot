#!/usr/bin/lua5.3

local sircbot   = require("sircbot")
local json      = require("cjson")
local http      = require("http.request")
local http_util = require ("http.util")
local mqtt      = require("mqtt.publish")

local sender  = arg[1]
local message = arg[2]
local channel = arg[3]
local me      = os.getenv("SIRCBOT_NICK") or "sircbot"
local gitlab  = "https://gitlab.alpinelinux.org"
local logdir  = "/var/www/html"

local function lmgtfy(q)
	local s = string.gsub(q, " ", "+")
	return "http://lmgtfy.com/?q="..s
end

local function get_body(url)
	local headers, stream = assert(http.new_from_uri(url):go())
	local body = assert(stream:get_body_as_string())
	local status = headers:get(":status")
	if status == "200" then
		return body
	end
end

local function gitlab_graphql(query)
	local req = http.new_from_uri(("%s/api/graphql"):format(gitlab))
	req.headers:upsert(":method", "POST")
	req.headers:append("content-type", "application/x-www-form-urlencoded")
	req:set_body(http_util.dict_to_query({query = query}))

	local headers, stream = assert(req:go())
	local body = assert(stream:get_body_as_string())
	local status = headers:get(":status")
	if status == "200" then
		return json.decode(body).data
	end
end

local function find_title(url)
	local html = get_body(url)
	return string.match(html or "", "<title>(.+)</title>")
end

local function aports_issue(id)
	return find_title("https://gitlab.alpinelinux.org/alpine/aports/issues/"..id)
end

local function gitlab_merge_request(id)
	local query = [[{
		project(fullPath: "alpine/aports") {
			mergeRequest(iid: "%s") {
				title
			}
		}
	}]]
	local mr = gitlab_graphql(query:format(id)).project.mergeRequest
	return ("%s: %s/alpine/aports/merge_requests/%d"):format(mr.title, gitlab, id)
end

local function gitlab_issue(id)
	local query = [[{
		project(fullPath: "alpine/aports") {
			issue(iid: "%s") {
				title
			}
		}
	}]]
	local issue = gitlab_graphql(query:format(id)).project.issue
	return ("%s: %s/alpine/aports/issues/%d"):format(issue.title, gitlab, id)
end

local function get_commit(id)
	local query = [[{
		project(fullPath: "alpine/aports") {
			repository {
				tree(ref: "%s") {
					lastCommit {
						title
					}
				}
			}
		}
	}]]
	local commit = gitlab_graphql(query:format(id)).project.repository.tree.lastCommit
	return ("%s: http://dup.pw/alpine/aports/%s"):format(commit.title, id:sub(1,12))
end

local function alpine_github_pr(id)
	return find_title("https://github.com/alpinelinux/aports/pull/"..id)
end

local function git_rebuild(branch)
	if branch:match("%d+%.%d+-stable") or branch:match("master") then
		local topic = ("git/aports/%s"):format(branch)
		mqtt.single(topic, "{\"id\":\"retry\"}", nil, nil, "msg.alpinelinux.org", 1883)
		return ("Retrying the %s branch"):format(branch)
	end
	return ("Sorry I dont know branch %s"):format(branch)
end

local function kick_abuild(branch)
	if branch:match("%d+%.%d+-stable") or branch:match("master") then
		local topic = ("git/aports/%s"):format(branch)
		mqtt.single(topic, "{\"id\":\"kick\"}", nil, nil, "msg.alpinelinux.org", 1883)
		return ("Kicking abuild on %s branch"):format(branch)
	end
	return ("Sorry I dont know branch %s"):format(branch)
end

local function kick_builder(builder)git@gitlab.alpinelinux.org:psykose/algitbot.git
	local branch
	if builder:match("build%-%d+%-%d+%-.+") then
		branch = ("%d.%d-stable"):format(builder:match("(%d+)-(%d+)"))
	elseif builder:match("build%-edge%-.+") then
		branch = "master"
	end
	if branch then
		local topic = ("git/aports/%s"):format(branch)
		local payload = ('{"id":"kick","builder":"%s"}'):format(builder)
		mqtt.single(topic, payload, nil, nil, "msg.alpinelinux.org", 1883)
		return ("Kicking builder %s"):format(builder)
	end
	return ("Sorry I dont know builder %s"):format(builder)
end

local function irclog(msg)
	local ym = os.date("%Y-%m")
	local log = ("%s/%s-%s.log"):format(logdir, channel, ym)
	local f = io.open(log, "a")
	if f == nil then return end
	f:write(("%s-%s %s\n"):format(ym, os.date("%d %H:%M:%S"), msg))
	f:close()
end

local function faq(keyword)
	local words = {
		["upgrade"] = "https://wiki.alpinelinux.org/wiki/Alpine_Linux:FAQ#How_do_I_upgrade_Alpine.3F",
	}
	if words[keyword] then
		return words[keyword]
	end
	return "http://wiki.alpinelinux.org/wiki/FAQ"
end

local l = {
	{ pattern = "ticket%s*(%d+)",
		callback = aports_issue
	},
	{ pattern = me..":%s*[Hh][Ii]",
		callback = function() return "hi there" end
	},
	{ pattern = "^[Hh]ello",
		callback = function() return "greetings" end
	},
	{ pattern = me..": hello there",
		callback = function() return "general kenobi" end
	},
	{ pattern = me..":%s*[Tt]hank",
		callback = function() return "you're welcome" end
	},
	{ pattern = me..":%s*[Ww]ho ",
		callback = function() return "sorry, I don't know" end
	},
	{ pattern = me..":%s*[Ww]hy ",
		callback = function() return "I wish i knew" end
	},
	{ pattern = me..":%s*[Ww]hat ",
		callback = function() return "please don't ask :)" end
	},
	{ pattern = me..":%s*[Hh]ow ",
		callback = function() return "I'm not sure..." end
	},
	{ pattern = me..":%s*you ",
		callback = function() return sender..": me? thanks! :)" end
	},
	{ pattern = "[Dd]rats",
		callback = function() return sender..": sorry about that" end
	},
	{ pattern = "[Ww][Oo0][Oo0][Tt]",
		callback = function() return sender..": congrats!" end
	},
	{ pattern = "\\o/",
		callback = function() return "\\o/" end
	},
	{ pattern = "faq (%w+)",
		callback = faq,
	},
	{ pattern = "lmgtfy (.*)",
		callback = lmgtfy
	},
	{ pattern = "^google (%w+.*)",
		callback = function(s) return "http://google.com/search?q="..string.gsub(s," ","+") end
	},
	{ pattern = "issue (%d+)",
		callback = aports_issue,
	},
	{ pattern = me..": i'm sorry",
		callback = function() return "it's ok. I forgive you" end
	},
	{ pattern = me..": please",
		callback = function() return "I will try do my best, "..sender end
	},
	{ pattern = me..": give me ",
		callback = function() return "I wish I could, "..sender end
	},
	{ pattern = me..": retry (%S+)",
		callback = git_rebuild
	},
	{ pattern = me..": kick (%S+)",
		callback = kick_builder
	},
	{ pattern = me..": ping",
		callback = function() return sender..": pong" end
	},
	{ pattern = "gitlab%.a%.o",
		callback = function() return "gitlab.a.o => https://gitlab.alpinelinux.org" end
	},
	{ pattern = "[Pp][Rr] ?#?(%d+)",
		callback = alpine_github_pr
	},
	{ pattern = "#(%d%d+)",
		callback = gitlab_issue
	},
	{ pattern = "!(%d%d+)",
		callback = gitlab_merge_request
	},
	{ pattern = string.rep("%w",40),
		callback = get_commit
	}
}

irclog(("<%s> %s"):format(sender, message))

for i in ipairs(l) do
	for arg in string.gmatch(message, l[i].pattern) do
		local ret = l[i].callback(arg)
		if ret then
			sircbot.connect(channel):send(ret)
		end
	end
end

