#!/bin/sh

set -euo pipefail

: "${SIRCBOT_OPTS:=$@}"
: "${SIRCBOT_NICK_PASSWORD:=}"
: "${SIRCBOT_FLUSH_RATE:=}"

die() {
	echo "$1" >&2
	exit 1
}

if [ "$SIRCBOT_FLUSH_RATE" ]; then
	SIRCBOT_OPTS="$SIRCBOT_OPTS -r $SIRCBOT_FLUSH_RATE"
fi

[ "$SIRCBOT_IRC_SERVER" ] || die "SIRCBOT_IRC_SERVER is not set, exiting."
[ "$SIRCBOT_NICK" ] || die "SIRCBOT_NICK is not set, exiting."

for dir in $SIRCBOT_CHANNELS; do
	mkdir -p /etc/sircbot.d/"$dir"
	ln -sf /usr/local/bin/sircbot.lua /etc/sircbot.d/"$dir"/default
done
	
exec /usr/bin/sircbot -f \
	-s "$SIRCBOT_IRC_SERVER" \
	-n "$SIRCBOT_NICK" \
	$SIRCBOT_OPTS \
	"${SIRCBOT_NICK_PASSWORD:+-c "nickserv :identify $SIRCBOT_NICK_PASSWORD"}" \
	$SIRCBOT_CHANNELS
