#!/bin/sh

set -euo pipefail

: "${MQTT_EXEC_HOST:=msg.alpinelinux.org}"
: "${MQTT_EXEC_SSL:=true}"
: "${MQTT_EXEC_TOPICS:=#}"
: "${MQTT_EXEC_OPTS:=}"

die() {
	echo "$1" >&2
	exit 1
}

if [ "$MQTT_EXEC_USER" ]; then 
	[ ! "$MQTT_EXEC_PASSWORD" ] && die "Cannot set user witout password"
	MQTT_EXEC_OPTS="$MQTT_EXEC_OPTS --username $MQTT_EXEC_USER"
fi

if [ "$MQTT_EXEC_PASSWORD" ]; then
	[ ! "$MQTT_EXEC_USER" ] && die "Cannot set password witout user"
	MQTT_EXEC_OPTS="$MQTT_EXEC_OPTS --password $MQTT_EXEC_PASSWORD"
fi

for topic in $MQTT_EXEC_TOPICS; do
	MQTT_EXEC_OPTS="$MQTT_EXEC_OPTS -t $topic"
done

case $MQTT_EXEC_SSL in
	[Tt][Rr][Uu][Ee])
		MQTT_EXEC_OPTS="$MQTT_EXEC_OPTS --port 8883 --cafile /etc/ssl/cert.pem" ;;
esac

exec mqtt-exec -v -h "$MQTT_EXEC_HOST" $MQTT_EXEC_OPTS -- "$@"
